package com.greensill.files;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FilenameUtils;

public class FileSystemWrapper {

	private FileSystem fileSystem = null;

	public FileSystemWrapper(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}
	
	/**
	 * Will create the directory of the pathStr and any parent directories if they don't already exist.
	 * 
	 * */
	public Path createDirAndParentDirs(String pathStr) throws IOException {
		Path path = fileSystem.getPath(pathStr);
		path = Files.createDirectories(path);
		
		return path;
	}
	
	/**
	 * Copies the contents of the resource at resourcePath to a new or existing file on the wrapped file system (fs), 
	 * will create the file and any parent directories if necessary.
	 * 
	 * @param filePath The absolute path to the file on the wrapped file system to which the contents of the resource
	 * 				   on resourcePath will be copied.
	 * @param resourcePath The path to the resource.
	 * @param classloader A ClassLoader that is able to find the resource at the resourcePath.
	 * 
	 * 
	 * */
	public void copyFileFromResource(String filePath, String resourcePath, ClassLoader classloader) throws IOException {
		String fileDirPath = FilenameUtils.getFullPath(filePath);
		String fileName = FilenameUtils.getName(filePath);
		copyFileFromResource(fileDirPath, fileName, resourcePath, classloader);
	}
	
	/**
	 * Copies the contents of the resource at resourcePath to a new or existing file on the wrapped file system (fs), 
	 * will create the file and any parent directories if necessary.
	 * 
	 * @param pathToFileDir The path to the directory of the file to which the contents of the resource will be copied
	 * @param fileName The file name of the file on the wrapped file system to which the contents of the resource will be copied
	 * @param resourcePath The path to the resource.
	 * @param classloader A ClassLoader that is able to find the resource at the resourcePath.
	 * 
	 * */
	public void copyFileFromResource(String pathToFileDir, String fileName, String resourcePath, ClassLoader classloader) throws IOException {
		try (InputStream istream = classloader.getResourceAsStream(resourcePath)){
			copyFileFromStream(pathToFileDir, fileName, istream);
		}
	}
	
	/**
	 * Copies the contents of srcIS to a new or existing file on the wrapped file system (fs), will create
	 * the file and any parent directories if necessary.
	 * 
	 * @param pathToFileDir The path to the directory of the file to which the contents of srcIS will be copied
	 * @param fileName The file name of the file on the wrapped file system to which the contents of srcIS will be copied
	 * @param srcIS The input stream with the contents to be copied. Is not closed, caller should close it.
	 * 
	 * */
	public void copyFileFromStream(String pathToFileDir, String fileName, InputStream srcIS) throws IOException {
		Path pathToFileDirPath = createDirAndParentDirs(pathToFileDir);
		Path pathToFile = pathToFileDirPath.resolve(fileName);
		Files.copy(srcIS, pathToFile, StandardCopyOption.REPLACE_EXISTING);
	}

	/**
	 * Gets the wrapped FileSystem object 
	 * 
	 * */
	public FileSystem getFileSystem() {
		return fileSystem;
	}

	/**
	 * Sets the wrapped FileSystem object 
	 * 
	 * */
	public void setFileSystem(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}
	
}
