package com.greensill.ftp.dto;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.greensill.utilities.files.FileUtilities;

public class FileTransferInfo {

	private String localDirPath;
	private String remoteDirPath;

	private boolean moveAfterTransfer;
	private String remoteDirPathToMoveAfterTransfer;
	private String remoteFileNameOfMovedAfterTransfer;
	
	public FileTransferInfo() {
		
	}

	public FileTransferInfo(String localDirPath, String remoteDirPath, boolean moveAfterTransfer,
			String remoteDirPathToMoveAfterTransfer, String remoteFileNameOfMovedAfterTransfer) {
		
		setLocalDirPath(localDirPath);
		setRemoteDirPath(remoteDirPath);
		setMoveAfterTransfer(moveAfterTransfer);
		setRemoteDirPathToMoveAfterTransfer(remoteDirPathToMoveAfterTransfer);
		setRemoteFileNameOfMovedAfterTransfer(remoteFileNameOfMovedAfterTransfer);
	}
	
	/**
	 * See setter docs...
	 * @see FileTransferInfo#setLocalDirPath(String)
	 * 
	 * */
	public String getLocalDirPath() {
		return localDirPath;
	}
	
	/**
	 * The local path to the directory of the downloaded file. 
	 * A same style separator as the one in the argument will be deduced and appended
	 * to the end. The argument will also be trimmed and normalized.
	 * 
	 * */
	public void setLocalDirPath(String localDirPath) {
		
		if(localDirPath == null) {
			this.localDirPath = null;
		}else {
			localDirPath = FileUtilities.appendSeparatorToPath(localDirPath);
			this.localDirPath = FilenameUtils.normalize(localDirPath, 
				FileUtilities.shouldUnixSeparatorBeUsedInThePath(localDirPath));
		}
	}

	/**
	 * See setter docs...
	 * @see FileTransferInfo#setRemoteDirPath(String)
	 * 
	 * */
	public String getRemoteDirPath() {
		return remoteDirPath;
	}
	
	/**
	 * The remote path to the directory of the file on the FTP server.
	 * A same style separator as the one in the argument will be deduced and appended
	 * to the end. The argument will also be trimmed and normalized.
	 * 
	 * */
	public void setRemoteDirPath(String remoteDirPath) {
		
		if(remoteDirPath == null) {
			this.remoteDirPath = null;
		}else {
			remoteDirPath = FileUtilities.appendSeparatorToPath(remoteDirPath);
			this.remoteDirPath = FilenameUtils.normalize(remoteDirPath, 
				FileUtilities.shouldUnixSeparatorBeUsedInThePath(remoteDirPath));
		}
	}
	
	/**
	 * See setter docs...
	 * @see FileTransferInfo#setMoveAfterTransfer(String)
	 * 
	 * */
	public boolean getMoveAfterRetrieval() {
		return moveAfterTransfer;
	}
	/**
	 * Specifies whether the file has to be moved to another folder on the remote SFTP server after the 
	 * download has finished (possibly to signal end of download) 
	 * 
	 * */
	public void setMoveAfterTransfer(boolean moveAfterRetrieval) {
		this.moveAfterTransfer = moveAfterRetrieval;
	}
	
	/**
	 * See setter docs...
	 * @see FileTransferInfo#setRemoteDirPathToMoveAfterTransfer(String)
	 * 
	 * */
	public String getRemoteDirPathToMoveAfterTransfer() {
		
		return remoteDirPathToMoveAfterTransfer;
	}
	
	/**
	 * Specifies the path to the directory that the file has to be moved to on the remote SFTP server after its 
	 * transfer has finished (possibly to signal end of download or upload). 
	 * A same style separator as the one in the argument will be deduced and appended to the end. The argument 
	 * will also be trimmed and normalized retaining the original path separator of the argument.
	 * 
	 * */
	public void setRemoteDirPathToMoveAfterTransfer(String remoteDirPathToMoveAfterTransfer) {
		
		if(remoteDirPathToMoveAfterTransfer == null) {
			this.remoteDirPathToMoveAfterTransfer = null;
		}else {
			remoteDirPathToMoveAfterTransfer = FileUtilities.appendSeparatorToPath(remoteDirPathToMoveAfterTransfer);
			this.remoteDirPathToMoveAfterTransfer = FilenameUtils.normalize(remoteDirPathToMoveAfterTransfer, 
				FileUtilities.shouldUnixSeparatorBeUsedInThePath(remoteDirPathToMoveAfterTransfer)); // will use unix sep if already used in the path or if path is empty
		}
		
	}
	
	/**
	 * See setter docs...
	 * @see FileTransferInfo#setRemoteFileNameOfMovedAfterTransfer(String)
     *
	 * */
	public String getRemoteFileNameOfMovedAfterTransfer() {
		return remoteFileNameOfMovedAfterTransfer;
	}
	
	/**
	 * Specifies the name that the file has to have after it is moved to pathToMoveAfterTransfer on the remote  
	 * SFTP server after the download has finished (possibly to signal end of download or upload). Could either
	 * be used to model the transfer of a single file or as a temporary placeholder for the next file to be 
	 * processed in a loop. Performs null safe trim on the argument.
	 * 
	 * @throws IllegalArgumentException In the case that the argument is null, empty or contains \ or /.
	 * 
	 * */
	public void setRemoteFileNameOfMovedAfterTransfer(String remoteFileNameOfMovedAfterTransfer) {
		
		remoteFileNameOfMovedAfterTransfer = StringUtils.trim(remoteFileNameOfMovedAfterTransfer);
		
		if(FileUtilities.isFilename(remoteFileNameOfMovedAfterTransfer)) {
			this.remoteFileNameOfMovedAfterTransfer = remoteFileNameOfMovedAfterTransfer;
		}else if(StringUtils.isEmpty(remoteFileNameOfMovedAfterTransfer)){
			this.remoteFileNameOfMovedAfterTransfer = null;
		}else{
			throw new IllegalArgumentException("The argument cannot contain \\ or / !");
		}
		
	}
	
	/**
	 * Generated property from remoteDirPathToMoveAfterTransfer and remoteFileNameOfMovedAfterTransfer
	 * i.e. properly concatenates them.
	 * 
	 * */
	public String getRemoteFilePathOfMovedAfterTransfer() {
		
		if(getRemoteDirPathToMoveAfterTransfer() == null) {
			return null;
		}else if(getRemoteFileNameOfMovedAfterTransfer() == null) {
			return getRemoteDirPathToMoveAfterTransfer();
		}else {
			return getRemoteDirPathToMoveAfterTransfer() + getRemoteFileNameOfMovedAfterTransfer(); 
			// Works because getRemoteDirPathToMoveAfterTransfer() will always end with a path separator.
		}
		
	}
	
	/**
	 * Sets remoteDirPathToMoveAfterTransfer and remoteFileNameOfMovedAfterTransfer by extracting
	 * their values from the remoteFilePathOfMovedAfterTransfer argument. </br></br>
	 * 
	 * This method assumes that the argument is a path to a file, thus if the argument is "f3" f3 is assumed to be a file name ...
	 * getRemoteFilePathOfMovedAfterTransfer will return "/f3" in this case...
	 * 
	 * */
	public void setRemoteFilePathOfMovedAfterTransfer(String remoteFilePathOfMovedAfterTransfer) {
		
		if(remoteFilePathOfMovedAfterTransfer == null) {
			setRemoteDirPathToMoveAfterTransfer(null);
			setRemoteFileNameOfMovedAfterTransfer(null);
			
			return;
		}else if(remoteFilePathOfMovedAfterTransfer.contains("\\") && remoteFilePathOfMovedAfterTransfer.contains("/")) {
	        	throw new IllegalArgumentException("The argument cannot contain both / and \\!");
	    }
		
		remoteFilePathOfMovedAfterTransfer = remoteFilePathOfMovedAfterTransfer.trim();
	
		setRemoteDirPathToMoveAfterTransfer(FilenameUtils.getFullPath(remoteFilePathOfMovedAfterTransfer));
		
		String fname = FilenameUtils.getName(remoteFilePathOfMovedAfterTransfer);
		fname = StringUtils.isEmpty(fname) ? null : fname;
		setRemoteFileNameOfMovedAfterTransfer(fname);
	}
	
}
