package com.greensill.ftp.dto;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.greensill.utilities.files.FileUtilities;

public class FileDownloadInfo extends FileTransferInfo {

	private String localFileName;
	// localFilePath is generated
	
	private String remoteFilePattern;
	// remoteFilePath is generated
	
	public FileDownloadInfo() {
		super();
	}
	
	public FileDownloadInfo(String localDirPath, String localFileName, String remoteDirPath, String remoteFilePattern,
			boolean moveAfterRetrieval, String remoteDirPathToMoveAfterRetrieval, String remoteFileNameOfMovedAfterRetrieval) {
		
		// FileTransferInfo related
		super(localDirPath, remoteDirPath, moveAfterRetrieval, remoteDirPathToMoveAfterRetrieval, 
				remoteFileNameOfMovedAfterRetrieval);
		
		// FileDownloadInfo related
		setLocalFileName(localFileName);
		setRemoteFilePattern(remoteFilePattern);
		
	}

	/**
	 * In the case that only one file is downloaded, it's useful to have this property
	 * if we need to store the downloaded file with a different name than the one
	 * on the upstream.
	 * 
	 * */
	public String getLocalFileName() {
		return localFileName;
	}
	
	/**
	 * Will perform null safe trim. See getter description...
	 * @see FileDownloadInfo#getLocalFileName()
	 * 
	 * */
	public void setLocalFileName(String localFileName) {
		
		localFileName = StringUtils.trim(localFileName);
		
		if(FileUtilities.isFilename(localFileName)) {
			this.localFileName = localFileName;
		}else if(StringUtils.isEmpty(localFileName)){
			this.localFileName = null;
		}else{
			throw new IllegalArgumentException("The argument cannot contain /, \\, *, ? !");
		}
	}
	
	/**
	 * Generated property from localDirPath and localFileName i.e. properly
	 * concatenates them.
	 * 
	 * */
	public String getLocalFilePath() {
		if(getLocalDirPath() == null) {
			return null;
		}else if(getLocalFileName() == null) {
			return getLocalDirPath();
		}else {
			return getLocalDirPath() + getLocalFileName(); 
			// Works because getLocalDirPath() will always end with a path separator.
		}
	}
	
	/**
	 * Sets localDirPath and localFileName by extracting their values from the 
	 * localFilePath argument.
	 * 
	 * */
	public void setLocalFilePath(String localFilePath) {
		
		if(localFilePath == null) {
			setLocalDirPath(null);
			setLocalFileName(null);
			
			return;
		}else if(localFilePath.contains("\\") && localFilePath.contains("/")) {
	        	throw new IllegalArgumentException("The argument cannot contain both / and \\!");
	    }
		
		localFilePath = localFilePath.trim();
		
		setLocalDirPath(FilenameUtils.getFullPath(localFilePath));
		String fname = FilenameUtils.getName(localFilePath);
		fname = StringUtils.isEmpty(fname) ? null : fname;
		setLocalFileName(fname);
	}
	
	/**
	 * Could either be a file name or a file pattern, depending on whether we
	 * want to download one or multiple files.
	 * 
	 * */
	public String getRemoteFilePattern() {

		return remoteFilePattern;
	}
	
	/**
	 * Will perform null safe trim. See getter description...
	 * @see FileDownloadInfo#getRemoteFilePattern()
	 * 
	 * */
	public void setRemoteFilePattern(String remoteFilePattern) {
		this.remoteFilePattern = StringUtils.trim(remoteFilePattern);
	}
	
	/**
	 * Generated property from remoteDirPath and remoteFilePattern i.e. properly
	 * concatenates them.
	 * 
	 * */
	public String getRemoteFilePath() {
		
		if(getRemoteDirPath() == null) {
			return null;
		}else if(getRemoteFilePattern() == null) {
			return getRemoteDirPath();
		}else {
			return getRemoteDirPath() + getRemoteFilePattern(); 
			// Works because getRemoteDirPath() will always end with a path separator.
		}
	}
	
	/**
	 * Sets remoteDirPath and remoteFilePattern by extracting their values from the 
	 * remoteFilePath argument.
	 * 
	 * */
	public void setRemoteFilePath(String remoteFilePath) {
		
		if(remoteFilePath == null) {
			setRemoteDirPath(null);
			setRemoteFilePattern(null);
			
			return;
		}else if(remoteFilePath.contains("\\") && remoteFilePath.contains("/")) {
	        	throw new IllegalArgumentException("The argument cannot contain both / and \\!");
	    }
		
		remoteFilePath = remoteFilePath.trim();
		
		setRemoteDirPath(FilenameUtils.getFullPath(remoteFilePath));
		String fname = FilenameUtils.getName(remoteFilePath);
		fname = StringUtils.isEmpty(fname) ? null : fname;
		setRemoteFilePattern(fname);
	}

	@Override
	public String toString() {
		return "FileDownloadInfo [localFileName=" + localFileName + ", remoteFilePattern=" + remoteFilePattern
				+ ", getLocalDirPath()=" + getLocalDirPath() + ", getRemoteDirPath()=" + getRemoteDirPath()
				+ ", getMoveAfterRetrieval()=" + getMoveAfterRetrieval() + ", getRemoteDirPathToMoveAfterTransfer()="
				+ getRemoteDirPathToMoveAfterTransfer() + ", getRemoteFileNameOfMovedAfterTransfer()="
				+ getRemoteFileNameOfMovedAfterTransfer() + ", getRemoteFilePathOfMovedAfterTransfer()="
				+ getRemoteFilePathOfMovedAfterTransfer() + "]";
	}
	
}
