package com.greensill.utilities.files;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.greensill.utilities.files.exceptions.InvalidFilePathException;

/**
 * Simple static file utilities
 *
 * @author GCUK-SD
 *
 */
public final class FileUtilities {
	
	public static final String UNIX_SEPARATOR = "/";
	public static final String WINDOWS_SEPARATOR = "\\";

	/**
	 * Delete file
	 *
	 * @param relFileName
	 */
	public static void deleteFile(String fileName, boolean reportFileNotFound) {

		File file = new File(fileName);
		if (file.exists()) {

			file.delete();
			if (file.exists()) {
				System.err.println("Unable to delete file: " + fileName);
			}
		} else {
			if (reportFileNotFound)
				System.err.println("File not found: " + fileName);
		}

	}

	/**
	 * Simple file checks
	 *
	 * @param fileName
	 * @return
	 */
	public static boolean fileExists(String fileName, boolean checkIsReadable) {

		File file = new File(fileName);
		if (!file.exists()) {
			return false;
		}
		if (checkIsReadable && (!file.canRead())) {
			return false;
		}

		// Any other check?

		return true;
	}

	/**
	 * Simple check if path exists/isFile/isReadable/isWriteable
	 *
	 * @param fileName
	 * @return Error message : empty if all OK
	 */
	public static String checkPathReadWrite(String pathName) {

		// Check incomingTransferPath: exists/isDir/read/browse/write
		File dir = new File(pathName);
		if (!dir.exists()) {
			return ("Unable to read directory (exists?): " + pathName);
		}
		if (!dir.isDirectory()) {
			return ("Unable to open path as directory (file?): " + pathName);
		}
		if (!dir.canRead()) {
			return ("Unable to read from directory (permissions?): " + pathName);
		}
		if (dir.listFiles() == null) {
			return ("Unable to read file list from directory: " + pathName);
		}
		if (!dir.canWrite()) {
			return ("Unable to write to directory (permissions?): " + pathName);
		}

		// If we get here, all OK
		return "";
	}
	
	/**
	 * Will concatenate basePath with pathToAppend. Flexible as far as null, blanks and different
	 * path separators are concerned. More specifically 
	 * 
	 * <ol>
	 * 	<li> It will override the path separator of pathToAppend with the path separator of basePath if basePath has one. </li>
	 *  <li> If basePath has no path separators it will try to infer the type of the separator from pathToAppend. </li>
	 *  <li> If either basePath or pathToAppend contains both unix and windows style separators then an exception is raised. </li>
	 *  <li> Note that no exception is raised in case e.g. base path has different style separator to pathToAppend. basePath simply overrides the ones in pathToAppend. </li>
	 *  <li> If no path separators can be inferred from basePath and pathToAppend then unix style separators are used. </li>
	 *  <li> No separators are appended to the end of the result and none are deleted either. </li>
	 * </ol>
	 * 
	 *  Some examples:
	 * 
	 * 
	 * 
	 * */
	public static String concatenatePaths(String basePath, String pathToAppend) {
		
		if(basePath == null) {
			return null;
		}else if(pathToAppend == null) {
			return basePath;
		}
		
		basePath = basePath.trim();
		pathToAppend = pathToAppend.trim();
		
		boolean useUnixSeparator = shouldUnixSeparatorBeUsedInThePaths(basePath, pathToAppend);
		
		if (!StringUtils.isBlank(basePath)) {

			basePath = FilenameUtils.normalize(basePath, useUnixSeparator);
			basePath = appendSeparator(basePath, useUnixSeparator); 
		}
		
		if (!StringUtils.isBlank(pathToAppend)) {

			pathToAppend = FilenameUtils.normalize(pathToAppend, useUnixSeparator);
			
			if( !StringUtils.isBlank(basePath) && 
				(
					pathToAppend.startsWith(UNIX_SEPARATOR) && useUnixSeparator ||
					pathToAppend.startsWith(WINDOWS_SEPARATOR) && !useUnixSeparator
				)) {
				
				pathToAppend = pathToAppend.substring(1, pathToAppend.length());
			}
		}
		
		return basePath + pathToAppend;
	}
	
	public static String appendSeparator(String path, boolean appendUnixSeparator) {
		
		if(path == null) {
			
			return path;
		} else if(appendUnixSeparator) {
			
			return path + (!path.endsWith(UNIX_SEPARATOR) ? UNIX_SEPARATOR : "");
		} else {
			
			return path + (!path.endsWith(WINDOWS_SEPARATOR) ? WINDOWS_SEPARATOR : "");
		}
		
	}
	

	/**
	 * Will return true if the argument contains a UNIX separator or no separator at all.
	 * Will return false if the argument contains a Windows separator.
	 * 
	 * @throws IllegalArgumentException if the path contains both UNIX and Windows style separators.
	 * 
	 * */
	public static boolean shouldUnixSeparatorBeUsedInThePaths(String basePath, String pathToAppend) {
		
		if(getTheSeparatorUsedInThePath(basePath) == null) {
			return shouldUnixSeparatorBeUsedInThePath(pathToAppend);
		} else if(getTheSeparatorUsedInThePath(pathToAppend) == null) {
			return shouldUnixSeparatorBeUsedInThePath(basePath);
		}
		
		return shouldUnixSeparatorBeUsedInThePath(basePath);
	}
	
	/**
	 * Will return true if the argument contains a UNIX separator or no separator at all.
	 * Will return false if the argument contains a Windows separator.
	 * 
	 * @throws IllegalArgumentException if the path contains both UNIX and Windows style separators.
	 * 
	 * */
	public static boolean shouldUnixSeparatorBeUsedInThePath(String path) {
		
		if(StringUtils.isEmpty(path)) {
			return true;
		}
		
		String separator = getTheSeparatorUsedInThePath(path);
		
		return separator == null || separator.equals(UNIX_SEPARATOR);
	}

	/**
	 *
	 * @param path
	 * @param fileName
	 * @return
	 * @throws InvalidFilePathException
	 */
	public static String getResolvedFilePath(String path, String fileName) throws InvalidFilePathException {

		if ((StringUtils.isEmpty(path)) || StringUtils.isEmpty(fileName)) {
			throw new InvalidFilePathException("path and filename cannot be null or empty");
		}

		String fullPathWithFilename = FilenameUtils.concat(path, fileName);

		try {
			Paths.get(path, fileName);
		} catch (InvalidPathException ipe) {
			throw new InvalidFilePathException(ipe.getMessage());
		}

		return fullPathWithFilename;
	}

	/**
	 *
	 * @param fileName
	 * @return String
	 * @throws IllegalArgumentException
	 */
	public static String getFileExtension(String fileName) throws IllegalArgumentException {
		String fileExtension = "";
		if (StringUtils.isNotEmpty(fileName)) {
			int index = fileName.lastIndexOf('.');
			if (index >= 0) {
				fileExtension = fileName.substring(index + 1);
			}
		} else {
			throw new IllegalArgumentException("Invalid file name");
		}
		return fileExtension;
	}

	/**
	 * Performs a comparison of the fileName with the filePattern like a file system style comparison.
	 * i.e. ? means any character and * any characters. </br>
	 * </br>
	 * Will not accept empty strings or directory paths as arguments!
	 * 
	 * @param fileName The file name. Cannot be empty or a directory path!
	 * @param filePattern The file pattern. Cannot be empty!
	 * */
	public static boolean doesFileNameMatchFilePattern(final String fileName, final String filePattern) {
		
		if(StringUtils.isEmpty(fileName) || StringUtils.isEmpty(filePattern) || !isFilename(fileName)) {
        	throw new IllegalArgumentException("Invalid arguments provided!");
        }
		
		Path path = Paths.get(fileName);
		return FileSystems.getDefault().getPathMatcher("glob:" + filePattern).matches(path);
        
    }
	
	/**
	 * Returns true if the argument does not contain /, \, *, ? and is not null or empty. False otherwise. 
	 * </br></br>
	 * Warning! Will return true for invalid windows file names e.g. as"""sdf.txt or as|df.txt
	 * because it's supposed to be as generic as possible. 
	 * */
	public static boolean isFilename(String fileName) {
		if(StringUtils.isEmpty(fileName)) {
			return false;
		}
		
		return StringUtils.containsNone(fileName, "/\\?*");
	}
	
	/** 
	 * Returns true if the argument does not contain /, \ and is not empty or null. False otherwise. 
	 * A file name can also be considered a file pattern...
	 * </br></br>
	 * Warning! Will return true for invalid windows file names e.g. as"""sdf.txt or as|df.txt
	 * because it's supposed to be as generic as possible. 
	 * */
	public static boolean isFilePattern(String filePattern) {
		if(StringUtils.isEmpty(filePattern)) {
			return false;
		}
		
		return StringUtils.containsNone(filePattern, "/\\");
	}
	
	/**
	 * Returns the separator used in the path argument e.g. /folder1/folder2/file.txt will return / .
	 * The assumption is that the argument is either a UNIX or a Windows style path...
	 * 
	 * Returns null if no separator is found or if path is an empty string.
	 * 
	 * @throws IllegalArgumentException if the path contains both UNIX and Windows style separators.
	 * 
	 * */
	public static String getTheSeparatorUsedInThePath(String path) {
		
		if(StringUtils.isNotEmpty(path)) {
			
			path = path.trim();
			
			if(path.contains("\\") && path.contains("/")) {
	        	throw new IllegalArgumentException("The argument cannot contain both / and \\!");
	        }
			
			int indOfSep = path.indexOf(UNIX_SEPARATOR);
			
			if(indOfSep >= 0) {
				return UNIX_SEPARATOR;
			}
			
			indOfSep = path.indexOf(WINDOWS_SEPARATOR);
			if(indOfSep >= 0) {
				return WINDOWS_SEPARATOR;
			}
			
		}
		
		return null;
	}
	
	/**
	 * Appends the separator used in the path argument to the argument. If no separator is found and
	 * the argument is not an empty string appends a UNIX style separator e.g. C:\folder1\folder2 
	 * will return C:\folder1\folder2\ and folder1 will return folder1/ . If the path already ends with 
	 * a separator returns path argument.
	 * 
	 * The assumption is that the argument is either a UNIX or a Windows style path...
	 * 
	 * */
	public static String appendSeparatorToPath(String path) {
		
		if(null != path) {
			
			path = path.trim();
			String separator = getTheSeparatorUsedInThePath(path);
			
			if(separator == null) {
				// Separator not found and the string is not empty. Assume that it is a relative UNIX path...
				return path + UNIX_SEPARATOR;
				
			}else if(!path.endsWith(separator)) {
				
				return path + separator;
			}
			
		}
		
		return path;
	}
	
	/**
	 * Get the path for the directory for temporary files on the current system to store files produced by tests. Path will end with path separator.
	 * 
	 * */
	public static String getTempFolderPath() {
		
		String tempDir = System.getProperty("java.io.tmpdir");
		return FileUtilities.appendSeparatorToPath(tempDir);
	}
	
	/**
	 * Will return the absolute path to the resource specified by pathToResource (which may be passed as a relative path).
	 * @throws URISyntaxException 
	 *  
	 * */
	public static String getAbsolutePathToResource(Class cs, String pathToResource) throws URISyntaxException {
		
		URL resource = cs.getResource(pathToResource);
		return Paths.get(resource.toURI()).toFile().getAbsolutePath();
	
	}

}
