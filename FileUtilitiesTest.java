package com.greensill.tradingmodule.util.files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.greensill.utilities.files.FileUtilities;

@RunWith(MockitoJUnitRunner.class)
public class FileUtilitiesTest {
	
	/** Simple diagnostic logging */
	private Logger logger = LogManager.getLogger(this.getClass());

	@Test
	public void testIsFilenameSuccess() {
		assertTrue(FileUtilities.isFilename("asdf"));
		assertTrue(FileUtilities.isFilename("asdf.txt"));
		assertTrue(FileUtilities.isFilename("asdf.txt.bak"));
	}
	
	@Test
	public void testIsFilenameFailure() {
		
		assertFalse(FileUtilities.isFilename("as/df.txt"));
		assertFalse(FileUtilities.isFilename("as\\df.txt"));
		assertFalse(FileUtilities.isFilename("as*df.txt"));
		assertFalse(FileUtilities.isFilename("as?df.txt"));
		assertFalse(FileUtilities.isFilename("a*s\\d/f?.txt"));
	}
	
	@Test
	public void testDoesFileNameMatchFilePatternError() {
		
		boolean caughtIllegalArg = false;
		try {
			FileUtilities.doesFileNameMatchFilePattern("","");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
		caughtIllegalArg = false;
		try {
			FileUtilities.doesFileNameMatchFilePattern("as/df","?");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
		caughtIllegalArg = false;
		try {
			FileUtilities.doesFileNameMatchFilePattern("asdf","");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
	}
	
	@Test
	public void testDoesFileNameMatchFilePatternSuccess() {
		
		assertTrue(FileUtilities.doesFileNameMatchFilePattern("1asdf","?asdf"));
		assertTrue(FileUtilities.doesFileNameMatchFilePattern("1123asdf","*asdf"));
		assertTrue(FileUtilities.doesFileNameMatchFilePattern("kkkkasdf1","*asdf?"));
		assertTrue(FileUtilities.doesFileNameMatchFilePattern("aLsLdf090909","a?s?df*"));
		
	}
	
	@Test
	public void testDoesFileNameMatchFilePatternFailure() {
		
		assertFalse(FileUtilities.doesFileNameMatchFilePattern("1aXdf","?asdf"));
		assertFalse(FileUtilities.doesFileNameMatchFilePattern("1123asdf123","*asdf"));
		assertFalse(FileUtilities.doesFileNameMatchFilePattern("kkkkasdf1","*asXXf?"));
		assertFalse(FileUtilities.doesFileNameMatchFilePattern("aLsLdf090909","a?s?df*x"));
		assertFalse(FileUtilities.doesFileNameMatchFilePattern("asdf","a?s?df*"));
		
	}
	
	@Test
	public void getTheSeparatorUsedInThePathSuccess() {
		assertEquals("/", FileUtilities.getTheSeparatorUsedInThePath("/"));
		assertEquals("/", FileUtilities.getTheSeparatorUsedInThePath("/f1/f2/a.txt"));
		assertEquals("/", FileUtilities.getTheSeparatorUsedInThePath("f1/f2/a.txt"));
		assertEquals("/", FileUtilities.getTheSeparatorUsedInThePath("/f1/f2/"));
		assertEquals("/", FileUtilities.getTheSeparatorUsedInThePath("f1/f2/"));
		
		assertEquals("\\", FileUtilities.getTheSeparatorUsedInThePath("\\"));
		assertEquals("\\", FileUtilities.getTheSeparatorUsedInThePath("\\f1\\f2\\a.txt"));
		assertEquals("\\", FileUtilities.getTheSeparatorUsedInThePath("f1\\f2\\a.txt"));
		assertEquals("\\", FileUtilities.getTheSeparatorUsedInThePath("\\f1\\f2\\"));
		assertEquals("\\", FileUtilities.getTheSeparatorUsedInThePath("f1\\f2\\"));
		
		assertEquals(null, FileUtilities.getTheSeparatorUsedInThePath("folder"));
		assertEquals(null, FileUtilities.getTheSeparatorUsedInThePath(""));
	}
	
	@Test
	public void getTheSeparatorUsedInThePathError() {
		
		boolean caughtIllegalArg = false;
		try {
			FileUtilities.getTheSeparatorUsedInThePath("\\f1/f2\\");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
		caughtIllegalArg = false;
		try {
			FileUtilities.getTheSeparatorUsedInThePath("\\f1/f2");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
	}
	
	@Test
	public void shouldUnixSeparatorBeUsedInThePathSuccess() {
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath("/"));
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath("/f1/f2/a.txt"));
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath("f1/f2/a.txt"));
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath("/f1/f2/"));
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath("f1/f2/"));
		
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath("folder"));
		assertTrue(FileUtilities.shouldUnixSeparatorBeUsedInThePath(""));
	}
	
	@Test
	public void shouldUnixSeparatorBeUsedInThePathFailure() {
		assertFalse(FileUtilities.shouldUnixSeparatorBeUsedInThePath("\\"));
		assertFalse(FileUtilities.shouldUnixSeparatorBeUsedInThePath("\\f1\\f2\\a.txt"));
		assertFalse(FileUtilities.shouldUnixSeparatorBeUsedInThePath("f1\\f2\\a.txt"));
		assertFalse(FileUtilities.shouldUnixSeparatorBeUsedInThePath("\\f1\\f2\\"));
		assertFalse(FileUtilities.shouldUnixSeparatorBeUsedInThePath("f1\\f2\\"));
	}
	
	@Test
	public void shouldUnixSeparatorBeUsedInThePathError() {
		
		boolean caughtIllegalArg = false;
		try {
			FileUtilities.shouldUnixSeparatorBeUsedInThePath("\\f1/f2\\");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
		caughtIllegalArg = false;
		try {
			FileUtilities.shouldUnixSeparatorBeUsedInThePath("\\f1/f2");
		} catch (IllegalArgumentException e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
	}
	
	@Test
	public void testAppendSeparatorToPathSuccess() {
		
		assertEquals("f3/", FileUtilities.appendSeparatorToPath("f3/"));
		assertEquals("f3/", FileUtilities.appendSeparatorToPath("f3"));
		assertEquals("f3/f.txt/", FileUtilities.appendSeparatorToPath("f3/f.txt")); // doesn't matter if it "looks like" a file name...
		assertEquals("/", FileUtilities.appendSeparatorToPath(""));
		assertEquals("/", FileUtilities.appendSeparatorToPath("\t \n"));
	}
	
	@Test
	public void testAppendSeparatorToPathError() {
		
		boolean caughtIllegalArg = false;
		try {
			FileUtilities.appendSeparatorToPath("\\f1/f2");
		} catch (Exception e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
		caughtIllegalArg = false;
		try {
			FileUtilities.appendSeparatorToPath("\\/");
		} catch (Exception e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
		caughtIllegalArg = false;
		try {
			FileUtilities.appendSeparatorToPath("f1/f2\\f3.txt");
		} catch (Exception e) {
			caughtIllegalArg = true;
		}
		assertTrue(caughtIllegalArg);
		
	}
	
	@Test
	public void testIsFilePatternSuccess() {
		
		assertTrue(FileUtilities.isFilePattern("1aXdf"));
		assertTrue(FileUtilities.isFilePattern("?1aXdf"));
		assertTrue(FileUtilities.isFilePattern("1*aXdf"));
		assertTrue(FileUtilities.isFilePattern("1aXdf?"));
		assertTrue(FileUtilities.isFilePattern("1aXdf*"));
		assertTrue(FileUtilities.isFilePattern("1 aX d ** f?"));
	}
	
	@Test
	public void testIsFilePatternFailure() {
		
		assertFalse(FileUtilities.isFilePattern("/1aXdf"));
		assertFalse(FileUtilities.isFilePattern("?1aXdf/"));
		assertFalse(FileUtilities.isFilePattern("\\1*aXdf"));
		assertFalse(FileUtilities.isFilePattern("1aXdf?\\"));
		assertFalse(FileUtilities.isFilePattern("\\1a\\Xdf*"));
		assertFalse(FileUtilities.isFilePattern("/1 aX/ d ** /f?"));
		assertFalse(FileUtilities.isFilePattern("/1 aX\\ d/ *\\* /f?"));
	}
	
	@Test
	public void testConcatenateFilePaths_Roots() {
		
		assertEquals(FileUtilities.concatenatePaths("", ""), "");
		assertEquals(FileUtilities.concatenatePaths("/", "/"), "/");
		assertEquals(FileUtilities.concatenatePaths("\\", "\\"), "\\");
		assertEquals(FileUtilities.concatenatePaths("/", "\\"), "/");
		assertEquals(FileUtilities.concatenatePaths("\\", "/"), "\\");
		assertEquals(FileUtilities.concatenatePaths("/", ""), "/");
		assertEquals(FileUtilities.concatenatePaths("\\", ""), "\\");
		assertEquals(FileUtilities.concatenatePaths("", "\\"), "\\");
		assertEquals(FileUtilities.concatenatePaths("", "/"), "/");
	}
	
	@Test
	public void testConcatenateFilePathsNonEmptySeparatorDicatedByBasePath() {
		
		assertEquals(FileUtilities.concatenatePaths("foo", "bar"), "foo/bar");
		assertEquals(FileUtilities.concatenatePaths("/foo", "bar"), "/foo/bar");
		assertEquals(FileUtilities.concatenatePaths("/foo", "/bar"), "/foo/bar");
		assertEquals(FileUtilities.concatenatePaths("/foo/", "bar"), "/foo/bar");
		assertEquals(FileUtilities.concatenatePaths("/foo", "/bar/"), "/foo/bar/");
		assertEquals(FileUtilities.concatenatePaths("/foo/", "/bar/"), "/foo/bar/");

	}
	
	@Test
	public void testConcatenateFilePathsNonEmptySeparatorDicatatedByPathToAppend() {
		
		assertEquals(FileUtilities.concatenatePaths("foo", "/bar"), "foo/bar");
		assertEquals(FileUtilities.concatenatePaths("foo", "\\bar"), "foo\\bar");
		
		assertEquals(FileUtilities.concatenatePaths("foo", "bar/"), "foo/bar/");
		assertEquals(FileUtilities.concatenatePaths("foo", "bar\\"), "foo\\bar\\");
		
	}
	
	@Test
	public void testConcatenateFilePathsNonEmptyRootsAndPaths() {
		
		assertEquals(FileUtilities.concatenatePaths("", "\\bar"), "\\bar");
		assertEquals(FileUtilities.concatenatePaths("", "/bar"), "/bar");
		
		assertEquals(FileUtilities.concatenatePaths("", "\\bar\\"), "\\bar\\");
		assertEquals(FileUtilities.concatenatePaths("", "/bar/"), "/bar/");
		
	}
	
	@Test
	public void testConcatenateFilePathsEndWithSeparator() {
		
		assertEquals(FileUtilities.concatenatePaths("", "bar\\"), "bar\\");
		assertEquals(FileUtilities.concatenatePaths("", "bar/"), "bar/");
		
		assertEquals(FileUtilities.concatenatePaths("bar\\", ""), "bar\\");
		assertEquals(FileUtilities.concatenatePaths("bar/", ""), "bar/");
		
		assertEquals(FileUtilities.concatenatePaths("  ", " bar\\	"), "bar\\");
		assertEquals(FileUtilities.concatenatePaths("	", " bar/"), "bar/");
		
		assertEquals(FileUtilities.concatenatePaths("bar\\", "  "), "bar\\");
		assertEquals(FileUtilities.concatenatePaths("bar/	", "		"), "bar/");
		
	}
	
	@Test
	public void testConcatenateFilePathsFail() {
		
		boolean excCaught = false;
		try {
			FileUtilities.concatenatePaths("", "\\bar/");
		} catch (IllegalArgumentException e) {
			excCaught = true;
		}
		
		assertTrue(excCaught);
		
		excCaught = false;
		
		try {
			FileUtilities.concatenatePaths("/bar\\", "");
		} catch (IllegalArgumentException e) {
			excCaught = true;
		}
		
		assertTrue(excCaught);
	}
	
	@Test
	public void testGetAbsolutePathToResource() throws URISyntaxException {

		String relativePath = "/liquibase/changelog/changelog_master.xml";
		String absolutePath = FileUtilities.getAbsolutePathToResource(this.getClass(), relativePath);
		logger.debug("absolutePath: {}", absolutePath);
		assertTrue(absolutePath.length() > relativePath.length());
		assertTrue(absolutePath.endsWith("changelog_master.xml"));
	}
	
}
